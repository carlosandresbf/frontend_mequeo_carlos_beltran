import { NgModule } from '@angular/core';
import { MyRoutes } from './routes/app.MyRoutes';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import {HttpClientModule, HttpHeaders, HTTP_INTERCEPTORS} from '@angular/common/http';

import { AppComponent } from './app.component';
import { NavigationComponent } from './template/navigation/navigation.component';
import { FootComponent } from './template/foot/foot.component';
import { LoginComponent } from './auth/login/login.component';
import { LogoutComponent } from './auth/logout/logout.component';
import { UserCreateComponent } from './auth/user/user-create/user-create.component';
import { UserListComponent } from './dashboard/index/user-list/user-list.component';
import { FileListComponent } from './dashboard/file/file-list/file-list.component';
import { FileUploadComponent } from './dashboard/file/file-upload/file-upload.component';
import {TokenStorage} from './auth/login/token-storage.service';

import { UsersService } from './services/users/users.service';
import { FilesService } from './services/files/files.service';
import { InterceptService } from './services/intercept.service';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FootComponent,
    LoginComponent,
    LogoutComponent,
    UserCreateComponent,
    UserListComponent,
    FileListComponent,
    FileUploadComponent,
  ],
  imports: [
    BrowserModule,
    MyRoutes,
    NgbModule,
    DataTablesModule,
    FormsModule,
    AngularFontAwesomeModule,
HttpClientModule
  ],
  providers: [
    UsersService,
    FilesService,
    TokenStorage,
    InterceptService,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: InterceptService,
			multi: true
		},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
