export interface AccessData {
id: number;
name: string;
lastname: string;
password: string;
email: string;
doc_types_id: number;
document: string;
accessToken: string;
refreshToken: string;
pic: string;
}
