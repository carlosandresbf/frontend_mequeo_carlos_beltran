import { Injectable } from '@angular/core';
import {Credential} from './credential';
import {BehaviorSubject, Observable, Subject, from, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {TokenStorage} from './token-storage.service';
import {AccessData} from './access-data';
import {environment} from '../../../environments/environment';
import {map, catchError, tap, switchMap} from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

API = environment.API;
public onCredentialUpdated$: Subject<AccessData>;

constructor(
  private http: HttpClient,
  private tokenStorage: TokenStorage) {
    this.onCredentialUpdated$ = new Subject();
  }



public login(credential: Credential): Observable<any> {
  return this.http.post<AccessData>(this.API.API_URL + this.API.API_ENDPOINT_LOGIN, credential, this.getHTTPHeader()).pipe(
    map((result: any) => {
      if (result instanceof Array) {
        return result.pop();
      }
      return result;
    }),
    tap(this.saveAccessData.bind(this)),
    catchError(this.handleError('login', []))
  );
}

public logout(refresh?: boolean): void {
  this.tokenStorage.clear();
  if (refresh) {
   location.reload(true);
  }
}


private saveAccessData(accessData: AccessData) {
  if (typeof accessData !== 'undefined') {
    this.tokenStorage
      .setAccessToken(accessData.accessToken)
      .setRefreshToken(accessData.refreshToken)
      .setDocumentType(accessData.doc_types_id)
      .setUser(accessData);
      console.log(accessData);
    this.onCredentialUpdated$.next(accessData);
  }
}

private handleError<T>(operation = 'operation', result?: any) {
  return (error: any): Observable<any> => {
    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead
    // Let the app keep running by returning an empty result.
    return from(result);
  };
}

getHTTPHeader() {
  return {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };
}

}
