import { Component, OnInit, ViewChild } from '@angular/core';
import {AuthService} from './auth.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
public model: any = {email: '', password: ''};
@ViewChild('f') f: NgForm;

  constructor(private authService: AuthService,
              private router: Router) { }

  ngOnInit() {
  }


  submit() {
        this.authService.login(this.model).subscribe(response => {
          if (typeof response.error === 'undefined') {
            this.router.navigate(['/index']);
            location.reload(true);
          } else {
            alert('Usuario incorreto');
          }
        });

    }

}
