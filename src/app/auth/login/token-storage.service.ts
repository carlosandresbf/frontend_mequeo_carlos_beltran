import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { AccessData } from './access-data';

@Injectable()
export class TokenStorage {
  /**
   * Get access token
   * @returns {Observable<string>}
   */
  public getAccessToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('accessToken');
    return of(token);
  }

  /**
   * Get refresh token
   * @returns {Observable<string>}
   */
  public getRefreshToken(): Observable<string> {
    const token: string = <string>localStorage.getItem('refreshToken');
    return of(token);
  }

  /**
   * Get user roles in JSON string
   * @returns {Observable<any>}
   */
  public getDocumentType(): Observable<any> {
    const document: any = localStorage.getItem('doc_types_id');
    try {
      return of(JSON.parse(document));
    } catch (e) {}
  }

  public getUser(): Observable<any> {
    const user: any = localStorage.getItem('user');
    try {
      return of(JSON.parse(user));
    } catch (e) {}
  }

  /**
   * Set access token
   * @returns {TokenStorage}
   */
  public setAccessToken(token: string): TokenStorage {
    localStorage.setItem('accessToken', token);

    return this;
  }

  /**
   * Set refresh token
   * @returns {TokenStorage}
   */
  public setRefreshToken(token: string): TokenStorage {
    localStorage.setItem('refreshToken', token);

    return this;
  }

  /**
   * Set user Document Type
   * @param document
   * @returns {TokenStorage}
   */
  public setDocumentType(document: any): any {
    if (document != null) {
      localStorage.setItem('doc_types_id', JSON.stringify(document));
    }

    return this;
  }

  public setUser(data: AccessData): any {
    if (data != null) {
      localStorage.setItem(
        'user',
        JSON.stringify({
          email: data.email,
          name: data.name,
          document: data.document,
          lastname: data.lastname,
          pic: data.pic,
          id: data.id
        })
      );
    }

    return this;
  }

  /**
   * Remove tokens
   */
  public clear() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('userRoles');
    localStorage.removeItem('user');
  }
}
