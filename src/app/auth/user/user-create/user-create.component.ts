import { Component, OnInit, ViewChild } from '@angular/core';
import {UsersService} from '../../../services/users/users.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {

  public usuarios: any = { name: '', lastname: '', email: '',  password: '', doc_types_id: '', document: ''};
  pict: File;
  public documents: any = {id: '', name: '', abb: ''};
  @ViewChild('f') f: NgForm;

  constructor(private userService: UsersService,
    private router: Router) { }

  ngOnInit() {
    this.userService.loadDocumenTypes(this.documents).subscribe(response => {
      if (typeof response.error === 'undefined') {
        this.documents = response.data;
        console.log(this.documents);
      } else {
        console.log('Error al cargar documentos');
      }
    });

  }

  onFileChanged(event) {
    this.pict = event.target.files[0];
  }

  submit() {
    this.userService.create(this.usuarios, this.pict).subscribe(response => {
      if (typeof response.error === 'undefined') {
        this.router.navigate(['/index']);
      } else {
        alert('Error');
      }
    });

}

}
