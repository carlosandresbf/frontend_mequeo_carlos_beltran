import { Component, OnInit } from '@angular/core';
import { FilesService } from './../../../services/files/files.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.css']
})
export class FileListComponent implements OnInit {

  API = environment.API;
  public file: any = { content: '', users_id: 0};

  dtOptions: any = {};

  constructor(private filesService: FilesService) { }

  ngOnInit() {


    this.filesService.loadfiles(this.file).subscribe(response => {
      if (typeof response.error === 'undefined') {
        this.file = response.data;
        console.log(this.file);
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 2,
          responsive: true,
          dom: 'Bfrtip',
          buttons: [
            'copy',
            'print',
            'excel'
                   ]
        };
      } else {
        console.log('Error al cargar Usuarios');
      }
    });
  }

}
