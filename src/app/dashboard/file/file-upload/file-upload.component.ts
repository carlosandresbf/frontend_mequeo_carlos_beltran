import { Component, OnInit } from '@angular/core';
import {FilesService} from '../../../services/files/files.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  pict: File;

  constructor(private fileService: FilesService,
    private router: Router) { }

  ngOnInit() {
  }


  onFileChanged(event) {
    this.pict = event.target.files[0];
  }

  submit() {
    this.fileService.upload(this.pict).subscribe(response => {
      if (typeof response.error === 'undefined') {
        this.router.navigate(['/file']);
      } else {
        alert('Error');
      }
    });

}

}
