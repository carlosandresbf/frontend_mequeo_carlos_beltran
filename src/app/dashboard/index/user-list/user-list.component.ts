import { Component, OnInit } from '@angular/core';
import {UsersService} from '../../../services/users/users.service';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  API = environment.API;

  public usuarios: any = { name: '', lastname: '', email: '',  doc_types_id: '', document: '', pic: ''};

  dtOptions: any = {};

  imgRoute = this.API.API_BASE_URL + '/';

  constructor(private userService: UsersService) {

    this.userService.loadUsers(this.usuarios).subscribe(response => {
      if (typeof response.error === 'undefined') {
        this.usuarios = response.data;
        console.log(this.usuarios);
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 2,
          responsive: true,
          dom: 'Bfrtip',
          buttons: [
            'copy',
            'print',
            'excel'
                   ]
        };
      } else {
        console.log('Error al cargar Usuarios');
      }
    });

   }


  ngOnInit() {



  }


}
