export class UsersModel {
  id: number;
  name: string;
  lastname: string;
  email: string;
  password: string;
  password_confirmation: string;
  doc_types_id: string;
  document: string;
  pic: string;
}
