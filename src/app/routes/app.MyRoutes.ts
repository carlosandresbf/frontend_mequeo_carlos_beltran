
import {Routes, RouterModule} from '@angular/router';
import { LoginComponent } from '../auth/login/login.component';
import { LogoutComponent } from './../auth/logout/logout.component';
import { UserCreateComponent } from '../auth/user/user-create/user-create.component';
import { FileListComponent } from '../dashboard/file/file-list/file-list.component';
import { FileUploadComponent } from '../dashboard/file/file-upload/file-upload.component';
import { UserListComponent } from '../dashboard/index/user-list/user-list.component';

const ROUTES: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'logout', component: LogoutComponent},
  {path: 'user/create', component: UserCreateComponent},
  {path: 'index', component: UserListComponent},
  {path: 'file/upload', component: FileUploadComponent},
  {path: 'file', component: FileListComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'login'},
];


export const MyRoutes = RouterModule.forRoot(ROUTES);
