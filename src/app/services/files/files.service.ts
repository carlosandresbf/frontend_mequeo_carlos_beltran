import { Injectable } from '@angular/core';import { BehaviorSubject, Observable, Subject, from, throwError } from 'rxjs';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { TokenStorage } from '../../auth/login/token-storage.service';
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FilesService {
  API = environment.API;
  API_URL = environment.API.API_URL + environment.API.API_DOCUMENT_PLAIN_URL;

constructor(private http: HttpClient) { }

public upload(file: File): Observable<any> {
  const data = {
    plain_file: file
  };
  const form_data = new FormData();
  form_data.append('plain_file', file, file.name);
  return this.http.post<any>(this.API_URL, form_data);
}

public loadfiles(types: any): Observable<any> {
  return this.http
    .get<any>(
      this.API.API_URL + this.API.API_DOCUMENT_PLAIN_URL,
      this.getHTTPHeader()
    )
    .pipe(
      map((result: any) => {
        if (result instanceof Array) {
          return result.pop();
        }
        return result;
      }),
      catchError(this.handleError('login', []))
    );
}


private handleError<T>(operation = 'operation', result?: any) {
  return (error: any): Observable<any> => {
    // TODO: send the error to remote logging infrastructure
    alert(error.message); // log to console instead
    // Let the app keep running by returning an empty result.
    return from(result);
  };
}


getHTTPHeader() {
  return {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
}

}
