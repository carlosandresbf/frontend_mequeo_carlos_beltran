import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, delay, tap } from 'rxjs/operators';
import { AuthService } from '../auth/login/auth.service';
import { UsersService } from './users/users.service';
import { FilesService } from './files/files.service';

@Injectable()
// TODO Mostar errores  500  por alertcompenetn
export class InterceptService implements HttpInterceptor {
  constructor(
    private usersService: UsersService,
    private filesService: FilesService,
    private authService: AuthService
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request.headers.get('noToken') !== 'noToken') {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        }
      });
    }
    return next.handle(request).pipe(
      tap(
        event => {
          if (event instanceof HttpResponse) {
            // console.log(event.status);
          }
        },
        error => {
          if (error.status === 401) {
            this.authService.logout();
            location.reload(true);
          }
          if (error.status === 500) {
          }
          const err = error.error.message || error.statusText;
          return throwError(error);
        }
      )
    );
  }
}
