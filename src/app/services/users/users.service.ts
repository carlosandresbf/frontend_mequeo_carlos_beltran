import { Injectable } from '@angular/core';
import { UsersModel } from '../../models/users/users-model';
import { BehaviorSubject, Observable, Subject, from, throwError } from 'rxjs';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
  HttpParams
} from '@angular/common/http';
import { TokenStorage } from '../../auth/login/token-storage.service';
import { map, catchError, tap, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  public model: UsersModel = {
    id: 0,
    name: '',
    lastname: '',
    email: '',
    password: '',
    password_confirmation: '',
    doc_types_id: '',
    document: '',
    pic: ''
  };
  public types: any = { id: 0, name: '', abb: '' };
  public onCreate$: Subject<UsersModel>;
  API = environment.API;
  API_URL = environment.API.API_URL + environment.API.API_USERS_URL;

  constructor(private http: HttpClient, private tokenStorage: TokenStorage) {
    this.onCreate$ = new Subject();
  }

  public loadDocumenTypes(types: any): Observable<any> {
    return this.http
      .get<any>(
        this.API.API_URL + this.API.API_DOCUMENT_TYPES_URL,
        this.getHTTPHeader()
      )
      .pipe(
        map((result: any) => {
          if (result instanceof Array) {
            return result.pop();
          }
          return result;
        }),
        catchError(this.handleError('login', []))
      );
  }

  public loadUsers(types: any): Observable<any> {
    return this.http
      .get<any>(
        this.API.API_URL + this.API.API_USERS_URL,
        this.getHTTPHeader()
      )
      .pipe(
        map((result: any) => {
          if (result instanceof Array) {
            return result.pop();
          }
          return result;
        }),
        catchError(this.handleError('login', []))
      );
  }

  public create(user: UsersModel, file: File): Observable<any> {
    const data = {
      user,
      pic: file
    };
    const form_data = new FormData();
    for (var key in user) {
      form_data.append(key, user[key]);
          }
    form_data.append('pic', file, file.name);
    return this.http.post<UsersModel>(this.API_URL, form_data);
  }

  private handleError<T>(operation = 'operation', result?: any) {
    return (error: any): Observable<any> => {
      // TODO: send the error to remote logging infrastructure
      alert(error.message); // log to console instead
      // Let the app keep running by returning an empty result.
      return from(result);
    };
  }

  getHTTPHeader() {
    return {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
  }
}
