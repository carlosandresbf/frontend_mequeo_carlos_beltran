import { Component, OnInit, OnChanges } from '@angular/core';
import {TokenStorage} from '../../auth/login/token-storage.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnChanges {
  session_user: any;

  constructor(
    private tokenStorage: TokenStorage) {
      this.session_user = this.tokenStorage.getUser();
  }

  ngOnChanges(session_users: any) {
    session_users = this.tokenStorage.getUser();
    console.log( session_users.value );
  }

}
