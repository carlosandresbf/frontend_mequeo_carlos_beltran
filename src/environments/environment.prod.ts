export const environment = {
  production: false,
  API: {
    API_URL: 'http://localhost:8000/api',
    API_BASE_URL: 'http://localhost:8000',
    API_ENDPOINT_LOGIN: '/auth/login',
    API_ENDPOINT_LOGOUT: '/auth/logout',
    API_USERS_URL: '/admin/user',
    API_DOCUMENT_TYPES_URL: '/admin/document_types',
    API_DOCUMENT_PLAIN_URL: '/admin/plain',
    API_ENDPOINT_REFRESH: '/auth/refresh',
    API_ENDPOINT_REGISTER: '/admin/user',
		API_PROFILE_IMAGES_URL: '/images/profile_pic'
  }
};
